---
title: Set Up a JavaScript Development Environment
date: 2016-12-10 19:37:03
tags: Web Development
categories: Internet
alias: jsdevenv
---
I have been using JavaScript for a number of years to manipulate web pages.  It's the primary way that we interact with Microsoft Dynamics CRM forms at my day job.  However, I've struggled with trying to get my head around building an application from the ground up in JavaScript.  I've looked at Angular 2 but there were so many gaps in my understanding that it was difficult to pick up.  Pluralsight, and specifically author Cory House, to the rescue.

<!-- more -->

Here's the rest of the story...