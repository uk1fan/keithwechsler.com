title: Raspberry Pi - Network Tunnel
date: 2018-01-08  19:00:00
tags: Raspberry Pi
categories: Utilites
---
I plan to use a number of different web utilites that will require my Raspberry Pi to temporarily be available from the Internet.  One of the easiest methods that I've found is to use ["ngrok"](https://ngrok.com) to provide a secure tunnel.  To expose an http service on a particular port, execute the program with the protocol and port:

{% codeblock "ngrok command" lang:shell %}
./ngrok http 1880
{% endcodeblock %}

This example will allow you to use Node-Red to develop a web service on your Raspberry Pi that can be used as a webhook on an Amazon Echo Alexa skill.