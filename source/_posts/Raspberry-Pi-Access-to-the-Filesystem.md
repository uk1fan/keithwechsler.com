title: Raspberry Pi - Access to the Filesystem
date: 2018-01-07  15:00:00
tags: Raspberry Pi
categories: Utilites
---
To make using the Raspberry Pi easier, I installed a couple of utilities.  I enabled VNC access so that I can open the Raspberry Pi's GUI from my MacBook and I also want to be able to access the Pi's filesystem directly.

<!-- more -->

The first thing that I recommend that you do is to give your Raspberry Pi a unique hostname and make sure that your router assigns a static IP address to this device.  This will allow you to access this computer in a consistent matter.

Access to the Raspberry Pi's filesystem from my MacBook will allow me to move and edit files from my primary machine.  An easy way to do this is to install the *Netatalk* utility that implements the AFP (Apple Filing Protocol) and provides native access through Finder.  To install netatalk:

{% codeblock "install netatalk" lang:shell %}
sudo appt-get install netatalk
{% endcodeblock %}

Then from Finder, just click on Go and Connect to Server:
![Connect to Server](/images/RaspberryPi/finder-menu-connect-to-server.png)

Enter, or choose, your Raspberry Pi and enter your credentials:
![Choose the Server](/images/RaspberryPi/finder-connect-to-server.png)

From the Shared section of the Finder Sidebar, you should see your Raspberry Pi. You can see my _raspberrypi3_ at the bottom of the list and the files from its Home directory on the right.  Navigate through it like you would any other part of your Macbook filesystem:
![Find Your Files](/images/RaspberryPi/finder-shared-raspberry-pi.png)

