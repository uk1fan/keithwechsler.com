title: Backup a Raspberry Pi
date: 2018-01-06  10:45:00
tags: Raspberry Pi
categories: Utilites
---
I've started using a Raspberry Pi 3 to learn some new technologies.  I want to be able to backup the files from the SD card used by the system because I don't want to lose the configure and startup files and have to start from scratch if something goes awry.

<!-- more -->

The easiest process that I found was to load the SD card into my MacBook, which has a SD slot, and just copy the image.  After inserting the card, you need to determine the name of the drive to be backed up.  From a bash prompt, use the *diskutil* utility to list the drives in your machine.

{% codeblock "diskutil command" lang:shell %}
diskutil list
{% endcodeblock %}

![diskutil](/images/RaspberryPi/diskutil.png)

From these results, you can see that 16 GB Linux disk is disk2.  You feed this value into the dd command and use the rdisk (raw disk) to speed up the operation:

{% codeblock "dd" lang:shell %}
sudo dd if=/dev/rdisk2 of=raspbian.stretch.setup bs=1m
{% endcodeblock %}

This will create the file *raspbian.stretch.setup* in the current directory:

![ls -al (before zip)](/images/RaspberryPi/file-before-zip.png)

To reduce the size of the backup, you can use the zip command to compress the file.

{% codeblock "zip" lang:shell %}
zip -r stretch.setup raspbian.stretch.setup
{% endcodeblock %}

![ls -al (after zip)](/images/RaspberryPi/file-after-zip.png)

You can see that the filesize has been deflated 86% from 16 GB to 2 GB.  The next step in the process would be to delete the original file.

I plan to try and write a Python script to automate this process.  Maybe you'll see a blog post on that process if I do.