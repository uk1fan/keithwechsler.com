title: A More Healthy Habitat
date: 2016-03-01 10:45:00
tags: Gadget
categories: Office
---
Like most people in my profession, I spend most of my waking hours behind a desk - normally 8+ hours at the office and 2-3 hours at home each evening.  I've suffered with lower back pain for a number of years and have recently switched to using a sit-stand desk both at work and at home to help alleviate some of the pain and stiffness.
{% blockquote James Levine, MD, PhD http://www.juststand.org/tabid/674/language/en-US/default.aspx juststand.org %}
Today, our bodies are breaking down from obesity, high blood pressure, diabetes, cancer, depression, and the cascade of health ills and everyday malaise that come from what scientists have named sitting disease.
{% endblockquote %}
<!-- more -->
At work, I had noticed a few adjustable workstations in various cubicles throughout our building and thought that one might be helpful to me.  I asked my boss about getting one and he immediately submitted a Service Request to have one ordered.  (Thanks, Steve!)  It turns out, that before our company will purchase one of these desks, you are required to fill out a survey about why you need one and be interviewed by someone from our Health and Wellness (Ergonomics) group.  

Once I completed these steps, an [Ergotron Workfit-S Dual Monitor Sit-Stand Workstation](http://www.ergotron.com/ProductsDetails/tabid/65/PRDID/378/language/en-US/Default.aspx)  showed up with the installers.  You can see a video of how it works on their website. 
{% youtube EV_i_mi---w %}

Here's my current setup:

![Work Desk Lowered](/images/Desk/Work-Lowered.jpg)

By pulling up on the keyboard tray, I can adjust it to a height where I can stand and work.  The vertical distance between the monitors and keyboard tray can be adjusted so that you have a comfortable viewing angle while typing.  Since I have bi-focals, I find it more comfortable to have the monitors lower than most people.

![Work Desk Raised](/images/Desk/Work-Raised.jpg)

I've had a lot less back pain from being able to modify my stance.  I normally stand about half of the time; it's most helpful to switch between standing and sitting because your legs and feet will suffer if you stand for too long.

I decided to use part of my bonus to purchase a stand-up desk for home.  I ordered an [Autonomous Desk](https://www.autonomous.ai/desk) and couldn't be happier.  I finally have room to setup both laptops and have some of my electronics gear accessible, too.  I've ordered a 28" Asus Ultra High Density (UHD) monitor and should receive it some time next week.  

![Home Desk Lowered](/images/Desk/Home-Lowered.jpg)

![Home Desk Raised](/images/Desk/Home-Raised.jpg)

I'll post an update once I get the monitor and cable management installed.  If you are using a sit-stand solution, leave a comment and let me know how it works for you.