---
title: Re-creating a Hexo Web Site
date: 2016-12-03 17:22:10
tags: Web Development
categories: Internet
---

I recently discovered that the files I need to create a post on this site had been lost.  Not only that, but pulling down the repo from my [bitbucket](https://bitbucket.org/uk1fan/keithwechsler.com/overview) repo didn't help.  Some of the critical files were not stored in the repo.  So, I've spent most of this weekend recreating the site and I took notes of the process and included some documentation to help with maintaining a Hexo site.

<!-- more -->

To create this site, I installed the latest version of [Hexo CLI](https://hexo.io/) on my Mac and created a new site:

{% codeblock %}
hexo init keithwechsler.com
{% endcodeblock %}

I then downloaded the latest [Icarus](http://blog.zhangruipeng.me/hexo-theme-icarus/) theme and placed it in the themes folder of the new site.  I then took remnants of the _config.yml files for the site and the theme from files I rescued from my old Mac and began editing the files until they displayed a site.  The configuration items had changed since I had originally created the blog, so I reviewed the example _config.yml files and made changes that I thought would make a difference. 

I then installed the npm modules and started the server which comes up on [http://0.0.0.0:4000](http://0.0.0.0:4000):

{% codeblock %}
yarn install
hexo server
{% endcodeblock %}

The most frustrating part of the setup was that I could not get the Tags and Categories pages to generate; each call to one of those pages produced an error.  The Icarus theme [documentation](https://github.com/ppoffice/hexo-theme-icarus/wiki/Theme) points out that you need to copy the tags and categories folders from the _source directory into your source directory, but I was copying them to the wrong destination.  They need to be placed in the site source directory outside of the theme folder structure.  I then re-read through the Hexo documentation and discovered that I needed to install the [hexo-generator-feed](https://github.com/hexojs/hexo-generator-feed) module and then everything worked fine.

To enable the new [Insight](https://github.com/ppoffice/hexo-theme-icarus/wiki/Search) search feature, I needed to install the [hexo-generator-json-content](hexo-generator-json-content) module:

{% codeblock %}
yarn add hexo-generator-json-content
{% endcodeblock %}

The final thing to trip me up was producing an atom-based RSS feed.  I discovered that I needed to add the [hexo-generator-feed](https://github.com/hexojs/hexo-generator-feed) module:

{% codeblock %}
yarn add hexo-generator-feed
{% endcodeblock %}

Once I had my environment re-created, I needed to be able to deploy the site to Github.  I had to install another module, [hexo-deployer-git](https://github.com/hexojs/hexo-deployer-git) and execute the command:

{% codeblock %}
yarn add hexo-deployer-git
hexo generate -deploy
{% endcodeblock %}

One thing you need to be careful about, if you decide to use these notes to setup a Hexo blog, is the .gitignore files and how the theme's .git files will be handled.  The reason I had to re-create my environment from scratch was because I had inadvertently deleted my themes folder which had been added to my blog repo as a submodule.  There is also a .gitignore file in the theme repo that says to ignore the _config.yml file which means that your repo does not have all of your configurations.  That's why I've decided to not try and maintain the two repos seperately and will probably need to perform the same steps in the future when Hexo or the theme is updated.

Now that I have an environment where I can produce new posts, I'm hopeful to begin blogging in earnest.  I know that I've said this before, I became bogged down about what I should post.  My approach going forward is to just write about what I'm learning and let the curation of the posts coming later.

## Maintaining the Site ##
Links to help with maintaining a Hexo blog:
- [Command-line commands](https://hexo.io/docs/commands.html)
- [Writing posts/pages](https://hexo.io/docs/writing.html)
- [Tag Plugins for posts](https://hexo.io/docs/tag-plugins.html)