---
title: Hosting a Static Site on Github
tags: Web Development
categories: Internet
date: 2016-12-06 23:40:17
---

I was talking with a friend today about working on my blog this weekend and how it was hosted on Github using Github Pages.  I told him that I would send him an e-mail with info on how to setup a site and I decided instead to create this post to document the steps I took.  Hopefully it may help others who are attempting to do the same thing.

<!-- more -->

# Github Setup #
The first step in the [process](https://pages.github.com/) is to create a repository in your Github account that matches the following format: username.github.io.  In my case, the repo needed to be named kwechsler.github.io since my username is kwechsler.  Within the repo's Settings, you'll need to indicate your custom domain:

![Custom Domain Settings](/images/StaticSiteConfig/GithubPages.png)

Once you have the repository created, clone it and copy it to your PC.  The first file you'll want to add to the repo is a CNAME file with your registered name.  The filename must be in all caps and spelled exactly like it is here.  This is a listing from my blog's repo where you can see the CNAME file in the root of the project:

![Project root files](/images/StaticSiteConfig/CNAME-File.png)

Inside the CNAME file, you need to list your domain.  You can see that I have the fully qualified domain name of http://www.keithwechsler.com.  The Github Pages documentation mentions having only the [bare domain or subdomain](https://help.github.com/articles/troubleshooting-custom-domains/#github-repository-setup-errors), but mine is working with the full URL:

![CNAME file contents](/images/StaticSiteConfig/CNAME-Contents.png)

You may also want to include a simple index.html HTML file so that once the configuration is complete, you'll have a page to display some data.  I recommend that this file display "Hello World!".  ;-)  Push these changes back to Github.

# DNS Configuration #
After you have your configuration specified on Github, you'll need to update the information on your DNS records.  I registered keithwechsler.com on Namecheap, so I went to the maintenance section of their site and updated the 'A' records with [Github's IP addresses](https://help.github.com/articles/setting-up-an-apex-domain/) and entered a www subdomain that points to the kwechsler.github.io repo - you'll use the one that you created on your DNS provider's site:

![DNS configuration on Namecheap](/images/StaticSiteConfig/DomainServerSetup.png)

It has been a year since I've made any changes to this configuration, but I believe that this is all you need to get started.  It takes some time for the DNS changes to propagate through the DNS system, but once that is complete, you are ready to start hosting some content.  If you are going to write a blog, and you want to generate a static site based on Markdown files, I suggest you check out [Hexo](https://hexo.io/).  Good luck!