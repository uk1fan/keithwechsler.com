title: Getting Started with AngularJS
date: 2016-02-28 21:45:33
tags: Web Development
categories: Internet
---
I have spent the last couple of weeks going through the ["AngularJS: Get Started"](https://www.pluralsight.com/courses/angularjs-get-started) course produced by [Scott Allen](http://www.odetocode.com) at Pluralsight.  It is a beginner-level course that walks you through the process of creating your first AngularJS single page application (SPA).  If you are familiar with creating a web page, and have some basic knowledge of JavaScript programming, you should be able to walk through the example with Scott and produce your own copy of the application.  

<!-- more -->

Since AngularJS is a client-side JavaScript framework, you don't have to have access to a server to work on most of the code.   Scott uses the web-based [Plunker](http://plnkr.co/) editor as he walks through the code, but I ran into issues getting it to work.  I believe that it may be related to differences in the JavaScript libraries between the time that Scott produced the course (June 2014) and now.  I did most of my initial development opening local files on my computer in the browser, but ran into issues when I attempted to use the ng-include directive because the browser was preventing a XSS (cross-site scripting) attack.  To get around this issue, I found the zero-configuration command-line [http-server](https://www.npmjs.com/package/http-server) that worked like a charm.  I intend to continue to use this tool for development purposes.  

The sample application is a GitHub Viewer.  It allows the user to enter a GitHub User ID and then retrieves a list of the public repositories maintained by the user on GitHub.

![GitHub Viewer Repo List](/images/GitHubViewer/Repos.jpg)

By clicking on one of the repos, the application displays the number of Open Issues and the Contributors for the selected repo.

![GitHub Viewer Contributors List](/images/GitHubViewer/Contributors.jpg)

Since I had never used AngularJS before, I went through each of the lessons exactly as Scott had outlined in the course.  There were a couple of places where the GitHub API had changed and it no longer matched the original content of the course, but Scott had placed updated information in the screenshots so that it was simple to understand the changes that needed to be made.  He also went over the use of the $interval command to implement a countdown time, but I chose to not implement that feature in my version of the app.  At the end of the course, Scott issued a challenge to implement the functionality to display the repo details and I was able to implement about 80% of it before looking at his solution.  There were a couple of items that I wasn't quite clear how to implement, but I felt good that I was able to complete as much as I did on my own.

My company has recently decided to move from the IBM Rational toolset (ClearCase / ClearQuest) for source control management to the Atlassain Git-based BitBucket Server and so I chose to store my application on their public site at [https://bitbucket.org](https://bitbucket.org/uk1fan/nggithubviewer).  I'm enjoying using Git and found BitBucket easy to setup and use.  I may modify the sample application to also support BitBucket since they provide a similar API to the one provided by GitHub.

This is an excellent course that I highly recommend if you are interested in learning about AngularJS.  I know that it has sparked my interest in AngularJS and I plan on choosing another course on this framework in the near future.  I've started listening to the [Adventures in Angular](https://devchat.tv/adventures-in-angular/) podcast and have found it to be interesting and entertaining.  The AngularJS team has recently released the beta version of Angular 2.0 and there will be plenty to learn in this new version.  I believe that I may choose to go through [Angular 2: First Look](https://www.pluralsight.com/courses/angular-2-first-look) course authored by [John Papa](http://johnpapa.net/) to get a head start on the new version.  I've had the pleasure of hearing John speak at a couple of Code Camps here in Florida and believe that this course will be a good investment of time.  John has been working on the [Tour of Heroes](https://angular.io/docs/ts/latest/tutorial/) Angular 2 tutorial and I plan to also work through it.  

If you decide to look at AngularJS, or you're already an expert, and come across any resources that you find beneficial, please leave a comment with the information and I'll check them out.  