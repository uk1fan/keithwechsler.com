---
title: JavaScript30
tags: Web Development
categories: Internet
date: 2016-12-08 23:02:06
---

I just completed the first project in the [#JavaScript30](https://javascript30.com) training course offered by Wes Bos.  He provides free access to 30 videos, starter files, and assets to "build 30 things in 30 days with 30 tutorials" using nothing but plain JavaScript.  In the first project, he walks you through building a "drum kit" in your browser.  Since most of my HTML/JavaScript experience is with line of business apps in a corporate setting, I had never used the audio HTML tag before.  I enjoyed Wes' presentation style and he may very well end up with another paying customer for his other courses.

The "drum kit" was a fun project and I'm looking forward to working on the other 29.  Wonder what's next???