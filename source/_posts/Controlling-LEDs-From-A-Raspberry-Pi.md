title: Controlling LEDs From a Raspberry Pi
date: 2016-03-08  21:45:00
tags: Raspberry Pi
categories: Electronics
---
A friend of ours called and asked my wife, Debbie, if we had a Raspberry Pi and knew how to use it.  Her son had a project due for school and needed to control about 20 LEDs - sequencing when they were on and off.  I had all of the hardware because I had played around with an Arduino and had bought some electronics parts that would be needed - breadboard, LEDs, resistors, and wires.  I had also purchased a [Pi Cobbler Plus](https://www.adafruit.com/products/2029) Breakout board to simplify connecting the Pi.  The only thing I had really done with the Pi to this point was copying the Raspian OS to an microSD memory card and booting the Pi.  Time for me to learn some Python and put the Pi to work.

<!-- more -->

I had never written any code in Python, so I went to the web to find some examples.  A friend from work had also sent me an e-mail with a couple of short routines to turn an LED on and off.  The project was to have a map of Europe and indicate different areas where battles were occuring during World War I.  The goal was to have the display to remain lit for an amount of time to indicate the duration of the battle.  I was able to spend a few minutes and came up with the following code:

```
#!/usr/bin/python 
import time
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

FIGHTING = GPIO.HIGH
OVER = GPIO.LOW

BATTLE01 = 4
BATTLE02 = 17
BATTLE03 = 22
BATTLE04 = 10
BATTLE05 = 9
BATTLE06 = 11
BATTLE07 = 5
BATTLE08 = 6

GPIO.setup(BATTLE01,GPIO.OUT)
GPIO.setup(BATTLE02,GPIO.OUT)
GPIO.setup(BATTLE03,GPIO.OUT)
GPIO.setup(BATTLE04,GPIO.OUT)
GPIO.setup(BATTLE05,GPIO.OUT)
GPIO.setup(BATTLE06,GPIO.OUT)
GPIO.setup(BATTLE07,GPIO.OUT)
GPIO.setup(BATTLE08,GPIO.OUT)

print("Lights test")
GPIO.setup(BATTLE01,GPIO.FIGHTING)
GPIO.setup(BATTLE02,GPIO.FIGHTING)
GPIO.setup(BATTLE03,GPIO.FIGHTING)
GPIO.setup(BATTLE04,GPIO.FIGHTING)
GPIO.setup(BATTLE05,GPIO.FIGHTING)
GPIO.setup(BATTLE06,GPIO.FIGHTING)
GPIO.setup(BATTLE07,GPIO.FIGHTING)
GPIO.setup(BATTLE08,GPIO.FIGHTING)

time.sleep(3)

print("Lights off")
GPIO.setup(BATTLE01,GPIO.OVER)
GPIO.setup(BATTLE02,GPIO.OVER)
GPIO.setup(BATTLE03,GPIO.OVER)
GPIO.setup(BATTLE04,GPIO.OVER)
GPIO.setup(BATTLE05,GPIO.OVER)
GPIO.setup(BATTLE06,GPIO.OVER)
GPIO.setup(BATTLE07,GPIO.OVER)
GPIO.setup(BATTLE08,GPIO.OVER)

while True:
    time.sleep(2)

    print("Start sequence...")
    GPIO.setup(BATTLE01,GPIO.FIGHTING)
    GPIO.setup(BATTLE02,GPIO.FIGHTING)
    GPIO.setup(BATTLE04,GPIO.FIGHTING)
    GPIO.setup(BATTLE05,GPIO.FIGHTING)
    GPIO.setup(BATTLE06,GPIO.FIGHTING)
    GPIO.setup(BATTLE07,GPIO.FIGHTING)
    
    time.sleep(2)
    GPIO.setup(BATTLE02,GPIO.OVER)
    GPIO.setup(BATTLE07,GPIO.OVER)
    GPIO.setup(BATTLE03,GPIO.FIGHTING)
    GPIO.setup(BATTLE08,GPIO.FIGHTING)
    
    time.sleep(2)
    GPIO.setup(BATTLE03,GPIO.OVER)
    
    time.sleep(2)
    GPIO.setup(BATTLE01,GPIO.OVER)
    GPIO.setup(BATTLE05,GPIO.OVER)

    time.sleep(2)
    GPIO.setup(BATTLE04,GPIO.OVER)

    time.sleep(2)
    GPIO.setup(BATTLE06,GPIO.OVER)
    GPIO.setup(BATTLE08,GPIO.OVER)

GPIO.setup(BATTLE01,GPIO.OVER)
GPIO.setup(BATTLE02,GPIO.OVER)
GPIO.setup(BATTLE03,GPIO.OVER)
GPIO.setup(BATTLE04,GPIO.OVER)
GPIO.setup(BATTLE05,GPIO.OVER)
GPIO.setup(BATTLE06,GPIO.OVER)
GPIO.setup(BATTLE07,GPIO.OVER)
GPIO.setup(BATTLE08,GPIO.OVER)

GPIO.cleanup()
print("End of module...")
```

The map was going to displayed on a standing kiosk and needed to run unattended.  I setup the Pi so that this Python app automatically ran once the Pi was turned on.  Although my friend's son never saw this code, it was a fun exercise for me to utilize the Pi and learn some Python in the process.