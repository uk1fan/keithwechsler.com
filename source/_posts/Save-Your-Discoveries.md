title: Save Your Discoveries
date: 2015-12-05 10:08:33
tags: Web
categories: Internet
---
I spend a lot of time "surfing the web" and come across a lot of information that I'd like to refer back to at a later time.  I've used a number of tools to try and keep up with all of this stuff and for the past four years I've been using [Pinboard](http://www.pinboard.in) because it is cross-platform and has the ability to add links through e-mail.  I was one of the earlier supporters of the site.  I've also purchased a couple of client apps to make finding and using these "Pinned" sites quicker.  However, the web interface of the Pinboard site is very dated and no frills.

Today I came across [kifi](https://www.kifi.com/) which is a website and an integrated plug-in for Chrome and Firefox that allows you to capture and collaborate on pages that you find on the web.  