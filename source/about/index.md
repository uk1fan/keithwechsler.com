---
title: About
date: 2015-11-17 00:50:37
comments: false
---
My name is Keith Wechsler. I am a 35-year career veteran of the [railroad](http://www.csx.com) industry and worked most recently as a software engineer in Jacksonville, FL.  My day job consisted of maintaining and enhancing a 1500-user, on-premise [Microsoft Dynamics CRM](https://www.microsoft.com/en-us/dynamics365/what-is-crm) system with a team of other developers and business analysts. I am currently looking for a new challenge where I can apply my skills and develop new ones.  If you are looking for a software engineer, please consider my [resume](/resource/PaulKeithWechslerII-Resume.pdf).

### NOTICE ###
The postings on this site are my own and do not necessarily represent any other party's positions, strategies or opinions. 